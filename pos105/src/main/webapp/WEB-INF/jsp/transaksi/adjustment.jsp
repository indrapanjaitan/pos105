<% request.setAttribute("contextName", request.getContextPath()); %>
<div id="modal-form" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="modal-title">Form Adjustment</h4>
			</div>
			<div class="modal-body">
				
			</div>
		</div>
	</div>
</div>

<!-- modal item  -->
<div id="modal-item" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 id="modal-title" class="modal-title">Add Item</h4>
			</div>
			<div class="modal-body">
				<%@include file="adjustment/add-item.jsp" %>
			</div>
		</div>
	</div>
</div>
<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Adjustment</h3>
		<div class="box-tools">
           <div class="input-group input-group-sm" style="width: 250px;">
             <input type="text" id="txt-search" class="form-control pull-right" placeholder="Search">

             <div class="input-group-btn">
               	<button type="button" id="btn-search" class="btn btn-default"><i class="fa fa-search"></i></button>
               	<button type="button" id="btn-add" class="btn btn-primary"><i class="fa fa-plus"></i></button>
             </div>
           </div>
         </div>
	</div>
	<div class="box-body">
		<table class="table table-considered">
			<thead>
				<tr>
					<td>Adjustment Date</td>
					<td>Notes</td>
					<td>Status</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody id="list-data">
			
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	function loadData(){
		$.ajax({
			url:'${contextName}/transaksi/adjustment/list',
			dataType:'html',
			type:'get',
			success:function(result){
				$("#list-data").html(result);
			}
		});
	}
	
	$(".date-picker").datepicker({
		autoclose: true,
		format:'mm/dd/yyyy',
	});
	
	$(document).ready(function(){
		// load data first display
		loadData();
		
		$("#btn-add").click(function(){
			$.ajax({
				url:'${contextName}/transaksi/adjustment/add',
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Menambah Adjustment Baru");
					$("#modal-form").removeClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		// add item
		$("#modal-form").on("click","#btn-add-item", function(){
			$("#modal-item").modal("show");
		});
		
		// search item variant
		$("#btn-item-search").click(function(){
			var keySearch = $("#txt-search-item").val();
			if(keySearch != null || keySearch==""){
				$.ajax({
					url:'${contextName}/master/item/searchVariant.json',
					data:{"key": keySearch},
					type:'get',
					dataType:'json',
					success:function(result){
						var dataVariant = "";
						$.each(result.list, function(index, item){
							dataVariant+='<tr>'+
									'<td>'+ item[1] +'</td>'+
									'<td>'+ item[5] +'</td>'+
									'<td><input type="text" class="form-control adjustment"/></td>'+
									'<td><button type="button" class="btn btn-success btn-xs btn-add-variant" data-varId="'+ item[0] +'" data-varName="'+ item[1] +'" data-itemId="'+ item[2]+'" data-itemName="'+ item[3] +'" data-inStock="'+ item[5] +'"><i class="fa fa-plus"></i></button> </td>'+
								'</tr>';
						});
						
						$("#list-data-item").html(dataVariant);
					}
				});
			}
		});
		
		// tombol plus diclick
		$("#list-data-item").on("click",".btn-add-variant", function(e){
			e.preventDefault();
			// define properti yang akan diambil		
			var variantId = $(this).attr("data-varId");
			var variantName = $(this).attr("data-varName");
			var itemId = $(this).attr("data-itemId");
			var itemName = $(this).attr("data-itemName");
			var inStock = $(this).attr("data-inStock");
			var adjustment = $(this).parent().parent().find(".adjustment").val();
			if(isNaN(adjustment) || adjustment == "" || Number(adjustment)==0){
				alert("Isi dahulu jumlah request ");
			} else {
				// mencari jumlah variant
				var row = $("#modal-form").find("#list-variant >tr").length;
				// definisikan data yang akan ditambahkan
				var data = '<tr id="'+ row +'">'+
						'<td>'+
							'<input type="hidden" id="detail_'+ row +'_variantId" name="detail['+ row +'].variantId" value="'+ variantId +'" class="form-control" /> '+
							'<input type="hidden" id="detail_'+ row +'_variantName" name="detail['+ row +'].variantName" value="'+ variantName +'" class="form-control" /> '+
							variantName +
						'</td>'+
						'<td><input type="text" id="detail_'+ row +'_inStockQty" name="detail['+ row +'].inStockQty" value="'+ inStock +'" class="form-control" /> </td>'+
						'<td><input type="text" id="detail_'+ row +'_actualStockQty" name="detail['+ row +'].actualStockQty" value="'+ adjustment +'" class="form-control" /> </td>'+
						'<td>'+
							'<button type="button" class="btn btn-danger btn-xs btn-delete-item"><i class="fa fa-trash-o"></i></button> '+
						'</td>'+
					'</tr>';
				// add ke list
				$("#modal-form").find("#list-variant").append(data);
			}
		});
		
		// tombol delete di klik
		$("#modal-form").on("click",".btn-delete-item", function(e){
			// remove row
			$(this).parent().parent().remove();
			
			$.each($("#modal-form").find("#list-variant >tr"), function(index, item){
				var trID = $(this).attr("id");
				// replace id
				$(this).attr('id',index);
				
				$(this).find(".form-control").each(function(key,val){
					// Replaced Name
                    var oldName = $(this).attr('name');
                   	var newName = oldName.replace('[' + trID + ']', '[' + index + ']');
                    $(this).attr('name', newName);
         
					// Replaced ID
                    var oldID = $(this).attr('id');
                    var newID = oldID.replace('_' + trID + '_', '_' + index + '_');
                    $(this).attr('id', newID);
                 
				});
			});
		});
		
		$("#modal-item").on("click","#btn-item-search",function(){
			var key = $("#modal-item").find("#txt-search-item").val();
			
			$.ajax({
				url:'${contextName}/master/item/listVariant',
				dataType:'html',
				type:'get',
				success:function(result){
					$("#list-data").html(result);
				}
			});
		});
		
		$("#btn-search").click(function(){
			var key = $("#txt-search").val();
			$.ajax({
				url:'${contextName}/Adjustment/search',
				data:{'key':key},
				dataType:'html',
				type:'get',
				success:function(result){
					$("#list-data").html(result);
				}
			});
		});
		
		$("#list-data").on('click','.btn-edit',function(){
			var id = $(this).val();
			$.ajax({
				url:'${contextName}/transaksi/adjustment/edit',
				data:{'id':id},
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Merubah Data Adjustment");
					$("#modal-form").removeClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		$("#list-data").on('click','.btn-delete',function(){
			var id = $(this).val();
			$.ajax({
				url:'${contextName}/transaksi/adjustment/delete',
				data:{'id':id},
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Menghapus Data Adjustment");
					$("#modal-form").addClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		// simpan data dari form
		$("#modal-form").on("submit","#form-adjustment", function(){
			$.ajax({
				url:'${contextName}/transaksi/adjustment/save.json',
				type:'post',
				data:$(this).serialize(),
				success:function(result){
					if(result.message=="success"){
						$("#modal-form").modal("hide");
						loadData();
					}
				}
			});				
			return false;
		});
	});
</script>