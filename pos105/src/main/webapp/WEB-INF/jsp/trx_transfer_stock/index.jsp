<html>
<head>
	<h1>Transfer Stock</h1>
</head>
<body>
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
			
				<div class="box-header">
					<div class="row">
						<div class="col-sm-12">
							
							<!-- left -->
							<div class="col-sm-2">
								<button type="button" class="btn btn-md btn-primary dropdown-toggle pull-left" data-toggle="dropdown-outlet" aria-expanded="true">
									To Outlet 
									<span class="fa fa-caret-down"></span>	
								</button>
							</div>
							
							<!-- right -->
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-3 col-md-2 col-lg-1 pull-right">
										<button type="button" class="btn btn-md btn-primary">Export</button>
									</div>
									<div class="col-sm-3 col-md-2 col-lg-1 pull-right">
										<button type="button" class="btn btn-md btn-primary">Create</button>
									</div>
								</div>
							</div>
						</div>
						
						
					</div>
				</div><!-- end of box-header -->
				
				<div class="box-body pad table-responsive">
					
				</div><!-- end of box-body -->
				
			</div><!-- end of box box-primary -->
		</div><!-- end of col-xs-12 -->
	</div><!-- end of row -->
</body>
</html>