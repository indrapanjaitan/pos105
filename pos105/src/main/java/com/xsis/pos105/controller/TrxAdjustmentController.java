package com.xsis.pos105.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.pos105.model.TrxAdjustmentModel;
import com.xsis.pos105.service.TrxAdjustmentService;
import com.xsis.pos105.service.TrxAdjustmentService;
import com.xsis.pos105.viewmodel.TrxAdjustmentViewModel;
import com.xsis.pos105.service.TrxAdjustmentService;

@Controller
public class TrxAdjustmentController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private TrxAdjustmentService service;

	@RequestMapping(value = "/transaksi/adjustment")
	public ModelAndView TrxAdjustmentIndex(Model model) {
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/transaksi/adjustment");
	}

	@RequestMapping(value = "/transaksi/adjustment/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) {
		Collection<TrxAdjustmentModel> result = null;
		try {
			result = this.service.get();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/transaksi/adjustment/list");
	}

	@RequestMapping(value = "/transaksi/adjustment/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) {
		String keySearch = request.getParameter("key");
		Collection<TrxAdjustmentModel> result = null;
		try {
			result = this.service.search(keySearch);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("list", result);

		return new ModelAndView("/transaksi/adjustment/list");
	}

	@RequestMapping(value = "/transaksi/adjustment/add", method = RequestMethod.GET)
	public ModelAndView add() {
		return new ModelAndView("/transaksi/adjustment/add");
	}

	@RequestMapping(value = "/transaksi/adjustment/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		TrxAdjustmentModel result = new TrxAdjustmentModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		// mengganti format datetime
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String readyTime = df.format(result.getCreatedOn());
		
		model.addAttribute("item", result);
		model.addAttribute("readyTime", readyTime);
		return new ModelAndView("/transaksi/adjustment/edit");
	}

	@RequestMapping(value = "/transaksi/adjustment/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		TrxAdjustmentModel result = new TrxAdjustmentModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/transaksi/adjustment/delete");
	}

	@RequestMapping(value = "/transaksi/adjustment/save")
	public String save(Model model, @ModelAttribute TrxAdjustmentViewModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		TrxAdjustmentModel tmp = null;
		try {
			tmp = this.service.getById(item.getId());

			if (action.equals("insert")) {
				item.setCreatedBy(1);
				item.setCreatedOn(new Date());
				item.setModifiedBy(1);
				item.setCreatedOn(new Date());
				
				// set outlet id
				item.setOutletId(1);
				
				// set status id
				item.setStatusId(1);
				
				this.service.insert(item);
			} else if (action.equals("update")){
				item.setModifiedBy(1);
				item.setCreatedOn(new Date());
				this.service.update(item);
			}				
			else if (action.equals("delete"))
				this.service.delete(tmp);

			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/transaksi/adjustment/save";
	}
}
