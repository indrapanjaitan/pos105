package com.xsis.pos105.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {


	private Log log = LogFactory.getLog(getClass());

	@RequestMapping("/home")
	public String home(Model model) {
		
		return "home";
	}

}
