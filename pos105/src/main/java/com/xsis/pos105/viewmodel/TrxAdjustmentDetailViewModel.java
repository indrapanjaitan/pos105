package com.xsis.pos105.viewmodel;

public class TrxAdjustmentDetailViewModel {
	private int id;
	private int adjustmentId;
	private int variantId;
	private String variantName;
	private int inStockQty;
	private int actualStockQty;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAdjustmentId() {
		return adjustmentId;
	}
	public void setAdjustmentId(int adjustmentId) {
		this.adjustmentId = adjustmentId;
	}
	public int getVariantId() {
		return variantId;
	}
	public void setVariantId(int variantId) {
		this.variantId = variantId;
	}
	public String getVariantName() {
		return variantName;
	}
	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}
	public int getInStockQty() {
		return inStockQty;
	}
	public void setInStockQty(int inStockQty) {
		this.inStockQty = inStockQty;
	}
	public int getActualStockQty() {
		return actualStockQty;
	}
	public void setActualStockQty(int actualStockQty) {
		this.actualStockQty = actualStockQty;
	}
}
