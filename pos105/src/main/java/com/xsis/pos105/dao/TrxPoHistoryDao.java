package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.model.TrxPoHistory;


public interface TrxPoHistoryDao {
	public Collection<TrxPoHistory> getAll() throws Exception;
	public TrxPoHistory getById(int id) throws Exception;
	public void save(TrxPoHistory trxPoHistory) throws Exception;
	public void update(TrxPoHistory trxPoHistory) throws Exception;
	public void delete(TrxPoHistory trxPoHistory) throws Exception;
}
