package com.xsis.pos105.dao;

import com.xsis.pos105.model.Petugas;

public interface EmployeeDao {
	
	public Petugas getByUserName(String email)throws Exception;

}
