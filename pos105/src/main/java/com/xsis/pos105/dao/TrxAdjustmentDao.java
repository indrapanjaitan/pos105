package com.xsis.pos105.dao;

import java.util.List;

import com.xsis.pos105.model.TrxAdjustmentModel;
import com.xsis.pos105.viewmodel.TrxAdjustmentViewModel;

public interface TrxAdjustmentDao {
	public List<TrxAdjustmentModel> get() throws Exception;
	public List<TrxAdjustmentModel> search(String keySearch) throws Exception;
	public TrxAdjustmentModel getById(int id) throws Exception;
	public void insert(TrxAdjustmentViewModel model) throws Exception;
	public void update(TrxAdjustmentViewModel model) throws Exception;
	public void delete(TrxAdjustmentModel model) throws Exception;
}
