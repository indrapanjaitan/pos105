package com.xsis.pos105.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.EmployeeDao;
import com.xsis.pos105.model.Petugas;

@Repository
public class EmployeeDaoImpl implements EmployeeDao{
	@Autowired
	private SessionFactory sessionFactory; 

	@Override
	public Petugas getByUserName(String email) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		Petugas result = (Petugas) session.createQuery("from Petugas where email = '" +email +"'").uniqueResult();
		return result;
	}
	
}
