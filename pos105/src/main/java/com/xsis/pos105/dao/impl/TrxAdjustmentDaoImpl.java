package com.xsis.pos105.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.TrxAdjustmentDao;
import com.xsis.pos105.model.TrxAdjustmentDetailModel;
import com.xsis.pos105.model.TrxAdjustmentHistoryModel;
import com.xsis.pos105.model.TrxAdjustmentModel;
import com.xsis.pos105.model.TrxPrDetailModel;
import com.xsis.pos105.model.TrxPrHistoryModel;
import com.xsis.pos105.viewmodel.TrxAdjustmentDetailViewModel;
import com.xsis.pos105.viewmodel.TrxAdjustmentViewModel;

@Repository
public class TrxAdjustmentDaoImpl implements TrxAdjustmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<TrxAdjustmentModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		List<TrxAdjustmentModel> result = session.createQuery("from TrxAdjustmentModel").list();
		return result;
	}

	@Override
	public List<TrxAdjustmentModel> search(String keySearch) throws Exception {		
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(TrxAdjustmentModel.class);
		criteria.add(Restrictions.like("name", "%" + keySearch + "%"));
		List<TrxAdjustmentModel> result = criteria.list();
		return result;
	}

	@Override
	public TrxAdjustmentModel getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		TrxAdjustmentModel result = session.get(TrxAdjustmentModel.class, id);
		return result;
	}

	@Override
	public void insert(TrxAdjustmentViewModel model) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		// header adjustment
		TrxAdjustmentModel header = new TrxAdjustmentModel();
		header.setOutletId(model.getOutletId());
		header.setNotes(model.getNotes());
		header.setStatusId(model.getStatusId());
		header.setCreatedBy(model.getCreatedBy());
		header.setCreatedOn(model.getCreatedOn());
		header.setModifiedBy(model.getModifiedBy());
		header.setModifiedOn(model.getModifiedOn());
		session.save(header);
		
		// history
		TrxAdjustmentHistoryModel history = new TrxAdjustmentHistoryModel();
		history.setAdjustmentId(header.getId());
		history.setStatusId(header.getStatusId());
		history.setCreatedBy(model.getCreatedBy());
		history.setCreatedOn(model.getCreatedOn());
		session.save(history);
		
		List<TrxAdjustmentDetailViewModel> listDetail = model.getDetail();
		if(listDetail != null && listDetail.size() > 0){
			for (TrxAdjustmentDetailViewModel item : listDetail) {
				TrxAdjustmentDetailModel detail = new TrxAdjustmentDetailModel();
				detail.setAdjustmentId(header.getId());
				detail.setVariantId(item.getVariantId());
				detail.setActualStockQty(item.getActualStockQty());
				detail.setInStockQty(item.getInStockQty());
				detail.setCreatedBy(model.getCreatedBy());
				detail.setCreatedOn(model.getCreatedOn());
				detail.setModifiedBy(model.getModifiedBy());
				detail.setModifiedOn(model.getModifiedOn());				
				session.save(detail);
			}
		}
	}

	@Override
	public void update(TrxAdjustmentViewModel model) throws Exception {
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(TrxAdjustmentModel model) throws Exception {
		sessionFactory.getCurrentSession().delete(model);
	}

}
