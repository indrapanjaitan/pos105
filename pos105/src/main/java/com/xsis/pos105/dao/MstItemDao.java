package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.model.MstItemModel;

public interface MstItemDao {

	public Collection<MstItemModel> getAll() throws Exception;
	public MstItemModel getById(int id) throws Exception;
	public void save(MstItemModel mstItem) throws Exception;
	public void update(MstItemModel mstItem) throws Exception;
	public void delete(MstItemModel mstItem) throws Exception;
}
