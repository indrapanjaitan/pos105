package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="POS_TRX_ADJUSTMENT_HISTORY")
public class TrxAdjustmentHistoryModel {
	private int id;
	private int adjustmentId;
	private int statusId;
	private Date createdOn;
	private int createdBy;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="TRX_ADJUSTMENT_HISTORY")
	@TableGenerator(name="TRX_ADJUSTMENT_HISTORY",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="TRX_ADJUSTMENT_HISTORY", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="ADJUSTMENT_ID")
	public int getAdjustmentId() {
		return adjustmentId;
	}
	public void setAdjustmentId(int adjustmentId) {
		this.adjustmentId = adjustmentId;
	}
	
	@Column(name="STATUS_ID")
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(name="CREATED_BY")
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
}
