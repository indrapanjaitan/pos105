package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "POS_MST_USER")
public class MstUser {

	private long id;
	private String username;
	private String password;
	private long roleId;
	private long employeeId;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	private boolean active;

	/**
	 * @return the id
	 */
	@Id
	@Column(name = "ID", length = 12, nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "User")
	@TableGenerator(name = "User", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "User", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	@Column(name = "USERNAME", length = 50)
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	@Column(name = "PASSWORD", length = 255)
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the roleId
	 */
	@Column(name = "ROLE_ID", length = 12)
	public long getRole_id() {
		return roleId;
	}

	/**
	 * @param roleId
	 *            the roleId to set
	 */
	public void setRole_id(long roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the employeeId
	 */
	@Column(name = "EMPLOYEE_ID", length = 12)
	public long getEmployee_id() {
		return employeeId;
	}

	/**
	 * @param employeeId
	 *            the employeeId to set
	 */
	public void setEmployee_id(long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the createdBy
	 */
	@Column(name = "CREATED_BY", length = 12, nullable = false)
	public long getCreated_by() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreated_by(long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATED_ON", nullable = false)
	public Date getCreated_on() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreated_on(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	@Column(name = "MODIFIED_BY", length = 12, nullable = false)
	public long getModified_by() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModified_by(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFIED_ON", nullable = false)
	public Date getModified_on() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn
	 *            the modifiedOn to set
	 */
	public void setModified_on(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Type(type = "org.hibernate.type.NumericBooleanType")
	@Column(name = "ACTIVE")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
