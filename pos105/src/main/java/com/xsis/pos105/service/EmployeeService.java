package com.xsis.pos105.service;

import com.xsis.pos105.model.Petugas;

public interface EmployeeService {
	
	public Petugas getByUserName(String email) throws Exception;

}
