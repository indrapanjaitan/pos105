package com.xsis.pos105.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.TrxPrDao;
import com.xsis.pos105.model.TrxPr;
import com.xsis.pos105.service.TrxPrService;

@Service
@Transactional
public class TrxPrServiceImpl implements TrxPrService{
	
	@Autowired
	private TrxPrDao trxPrDao;
	
	@Override
	public Collection<TrxPr> getAll() throws Exception {
		// TODO Auto-generated method stub
		return trxPrDao.getAll();
	}

	@Override
	public TrxPr getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return trxPrDao.getById(id);
	}

	@Override
	public void save(TrxPr trxPr) throws Exception {
		// TODO Auto-generated method stub
		trxPrDao.save(trxPr);
	}

	@Override
	public void update(TrxPr trxPr) throws Exception {
		// TODO Auto-generated method stub
		trxPrDao.update(trxPr);
	}

	@Override
	public void delete(TrxPr trxPr) throws Exception {
		// TODO Auto-generated method stub
		trxPrDao.delete(trxPr);
	}

}
