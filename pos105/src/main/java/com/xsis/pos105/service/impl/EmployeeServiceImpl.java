package com.xsis.pos105.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.EmployeeDao;
import com.xsis.pos105.model.Petugas;
import com.xsis.pos105.service.EmployeeService;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	EmployeeDao employeeDao;

	@Override
	public Petugas getByUserName(String email) throws Exception {
		// TODO Auto-generated method stub
		return employeeDao.getByUserName(email);
	}

}
