package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.TrxPrHistoryModel;

public interface TrxPrHistoryService {
	public Collection<TrxPrHistoryModel> getAll() throws Exception;
	public TrxPrHistoryModel getById(int id) throws Exception;
	public void save(TrxPrHistoryModel prhistory) throws Exception;
	public void update(TrxPrHistoryModel prhistory) throws Exception;
	public void delete(TrxPrHistoryModel prhistory) throws Exception;

}
