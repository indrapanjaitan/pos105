package com.xsis.pos105.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.TrxAdjustmentDao;
import com.xsis.pos105.model.TrxAdjustmentModel;
import com.xsis.pos105.service.TrxAdjustmentService;
import com.xsis.pos105.viewmodel.TrxAdjustmentViewModel;

@Service
@Transactional
public class TrxAdjustmentServiceImpl implements TrxAdjustmentService {
	
	@Autowired private TrxAdjustmentDao dao;
	
	@Override
	public List<TrxAdjustmentModel> get() throws Exception {
		return this.dao.get();
	}

	@Override
	public List<TrxAdjustmentModel> search(String keySearch) throws Exception {
		return this.dao.search(keySearch);
	}

	@Override
	public TrxAdjustmentModel getById(int id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public void insert(TrxAdjustmentViewModel model) throws Exception {
		this.dao.insert(model);
	}

	@Override
	public void update(TrxAdjustmentViewModel model) throws Exception {
		this.dao.update(model);
	}

	@Override
	public void delete(TrxAdjustmentModel model) throws Exception {
		this.dao.delete(model);
	}

}
