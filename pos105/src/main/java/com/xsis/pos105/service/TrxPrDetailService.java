package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.TrxPrDetailModel;

public interface TrxPrDetailService {
	public Collection<TrxPrDetailModel> getAll() throws Exception;
	public TrxPrDetailModel getById(int id) throws Exception;
	public void save(TrxPrDetailModel prdetail) throws Exception;
	public void update(TrxPrDetailModel prdetail) throws Exception;
	public void delete(TrxPrDetailModel prdetail) throws Exception;

}
